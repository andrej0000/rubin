all:
	bnfc -haskell RUBIN.cf
	happy -gca ParRUBIN.y
	alex -g LexRUBIN.x
	ghc --make TestRUBIN.hs -o TestRUBIN
	ghc --make RUBINInterpreter.hs -o RUBIN

clean:
	-rm -f *.log *.aux *.hi *.o *.dvi
	-rm -f DocRUBIN.ps
	-rm -f RUBIN

distclean: clean
	-rm -f DocRUBIN.* LexRUBIN.* ParRUBIN.* LayoutRUBIN.* SkelRUBIN.* PrintRUBIN.* TestRUBIN.* AbsRUBIN.* TestRUBIN ErrM.* SharedString.* ComposOp.* RUBIN.dtd XMLRUBIN.* 
	

