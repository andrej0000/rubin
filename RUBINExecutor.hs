module RUBINExecutor where

import Control.Monad
import Control.Monad.Trans.State
import Control.Monad.Trans.Reader
import AbsRUBIN
import RUBINTypeCheck

data Object = 
	  Lambda Env EIdent EStmt
	| InstOf ETypeConstIdent [Object]
	| LitInt Integer
	| BoolV  Bool
	deriving (Eq,Show)

instance Show (Env) where
	show e = ""

instance Eq (Env) where
	(==) (_) (_) = False

type IdObj = EIdent -> Maybe Object

data Env = Env { io :: IdObj }
--
fix = (ELamb eps g 
	(EApp 
	(ELamb eps x (EApp gs (EApp xs xs))) 
	(ELamb eps x (EApp gs (EApp xs xs)))))
	where
		eps = ELitType (ETypeIdent "_")
		g = EIdent "g"
		x = EIdent "x"
		gs = EVar g
		xs = EVar x

addMap :: EIdent -> Maybe Object -> Env -> Env
addMap id obj env = 
	(Env (\i -> if id == i then obj else ((io env) i)))

addMapList :: [EIdent] -> [Maybe Object] -> Env -> Env
addMapList [] [] env = env
addMapList (id:tid) (mo:tmo) env =
	addMap id mo (addMapList tid tmo env)

execute x = runReader (execEProg x) (Env (\_ -> Nothing))

execEProg :: EProg -> Reader Env (Maybe Object)
execEProg (Stmt e) = do
	e' <- execEStmt e
	return e'
execEProg (Decl (EDecl i v) eprog) = do
	vt <- execEStmt v
	st <- local (addMap i vt) (execEProg eprog)
	return st

execEProg (Recd _ funid argid stmt eprog) = 
	let eps = ELitType (ETypeIdent "_") in do
	vt <- execEStmt (EApp (fix) (ELamb eps funid (ELamb eps argid stmt)))
	st <- local (addMap funid vt) (execEProg eprog)
	return st


execEProg (Type etype eprog) = do
	t <- execEProg eprog
	return t

execEStmt :: EStmt -> Reader Env (Maybe Object)

execEStmt (EIf c t f) = do
	tc <- execEStmt c
	case tc of
		Nothing -> return Nothing
		Just (BoolV b) -> if b then execEStmt t else execEStmt f
		_ -> return Nothing

execEStmt (ELet (EDecl i v) s) = do
	vt <- execEStmt v
	st <- local (addMap i vt) (execEStmt s)
	return st

execEStmt (ELamb etype eident estmt) = do
	env <- ask
	return $ Just $ Lambda env eident estmt

execEStmt (EApp e1 e2) = do
	f <- execEStmt e1
	a <- execEStmt e2
	case f of 
		Just (Lambda env eident estmt) -> 
			local (\_ -> addMap eident a env) (execEStmt estmt)
		_ -> return Nothing
			
execEStmt (EPar estmt) = do
	t <- execEStmt estmt
	return t

execEStmt (ENot e1) = do
	v <- execEStmt e1
	return (do
		v' <- v
		case v' of 
			(BoolV b) -> Just (BoolV (not b))
			_ -> Nothing)

execEStmt (ECmp e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do	
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> if (x==y) then Just (BoolV True) else Just (BoolV False)
			_ -> Nothing)

execEStmt (EGrt e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do	
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> if (x>y) then Just (BoolV True) else Just (BoolV False)
			_ -> Nothing)

execEStmt (ELes e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do	
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> if (x<y) then Just (BoolV True) else Just (BoolV False)
			_ -> Nothing)

execEStmt (EGEq e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do	
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> if (x>=y) then Just (BoolV True) else Just (BoolV False)
			_ -> Nothing)


execEStmt (ELEq e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do	
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> if (x<=y) then Just (BoolV True) else Just (BoolV False)
			_ -> Nothing)

execEStmt (EBool bv) = 
	case bv of
		BVTrue 	-> do return $ Just (BoolV True)
		BVFalse -> do return $ Just (BoolV False)

execEStmt (EAnd e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do	
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((BoolV b1), (BoolV b2)) -> Just (BoolV (b1 && b2))
			_ -> Nothing)

execEStmt (EOr e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do	
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((BoolV b1), (BoolV b2)) -> Just (BoolV (b1 || b2))
			_ -> Nothing)


execEStmt (EAdd e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> Just (LitInt (x+y))
			_ -> Nothing)

execEStmt (ESub e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> Just (LitInt (x-y))
			_ -> Nothing)

execEStmt (EMul e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> Just (LitInt (x*y))
			_ -> Nothing)

execEStmt (EDiv e1 e2) = do
	v1 <- execEStmt e1
	v2 <- execEStmt e2
	return (do
		v1' <- v1
		v2' <- v2
		case (v1', v2') of
			((LitInt x), (LitInt y)) -> Just (LitInt (floor ((toRational x) / (toRational y))))
			_ -> Nothing)

execEStmt (EInt i) = do
	return $ Just $ LitInt i

execEStmt (EVar id) = do
	env <- ask
	return $ (io env) id

execEStmt (EInstS etci) = do
	return (Just (InstOf etci []))

execEStmt (EInstM etci l) = do
	l' <- mapM (execEStmt) l
	return (case sequence l' of
		Nothing -> Nothing
		Just l'' -> Just (InstOf etci l''))

execEStmt (ECase estmt matchlist) = do
	obj <- execEStmt estmt
	case obj of
		Nothing -> return Nothing
		Just obj' -> execECase obj' matchlist
	
execECase :: Object -> [EMatch] -> Reader Env (Maybe Object)
execECase _ [] = do return Nothing
execECase (InstOf const args) ((EMatchS const' estmt):t) = 
	if (const' == const) then
		execEStmt estmt
	else
		(execECase (InstOf const args) (t))

execECase (InstOf const args) ((EMatchM const' idents estmt):t) = 
	if (const' == const) then
		local (addMapList idents (map (Just) args)) (execEStmt estmt)
	else
		(execECase (InstOf const args) (t))
