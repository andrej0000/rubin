module RUBINTypeCheck where

import Control.Monad.Trans.State
import Control.Monad.Trans.Reader
import AbsRUBIN

-- UTIL FNC {{{
pushMaybe :: Maybe [a] -> [Maybe a]
pushMaybe (Nothing) = []
pushMaybe (Just l) = map (Just) l

isSame :: (Eq a) => [a] -> Maybe a
isSame [] = Nothing
isSame (h:t) =
	foldl (\acc new -> acc >>= (\t' -> if t' == new then Just t' else Nothing)) (Just h) t
-- }}}


data TypeCheck = TypeOk ECompType | TypeErr String

type ParentType = ETypeConstIdent -> (Maybe ETypeIdent) 
type CompList   = ETypeConstIdent -> (Maybe [ECompType])
type IdentType  = EIdent -> (Maybe ECompType)

data TypeCheckEnv = TE	{ parentType :: ParentType
			, compList   :: CompList
			, identType  :: IdentType
			}

type TypeState = EIdent -> Maybe ECompType

-- TypeCheckEProg {{{
typeCheckEProg :: EProg -> (Maybe ECompType)
typeCheckEProg eprog =
	runReader (typeCheckMEProg eprog) (TE (\_ -> Nothing) (\_ -> Nothing) (\_ -> Nothing))

typeCheckMEProg :: EProg -> Reader TypeCheckEnv (Maybe ECompType)
typeCheckMEProg (Stmt stmt) = do
	t <- typeCheckMEStmt stmt
	return t

typeCheckMEProg (Decl (EDecl i v) eprog) = do
	vt <- typeCheckMEStmt v
	st <- local (modifyIdentType (addIdentType vt i)) (typeCheckMEProg eprog)
	return (do
		vt' <- vt
		st' <- st
		return st')

typeCheckMEProg (Recd t funid argid estmt eprog) = do 
	st <- local (modifyIdentType (addIdentType (Just t) funid)) (typeCheckMEProg eprog)
	return (do 
		st' <- st
		return st')

typeCheckMEProg (Type etd eprog) = do
	t <- local (typeDef etd) (typeCheckMEProg eprog)
	return t
-- }}}

-- MODIFY {{{
modifyParentType :: (ParentType -> ParentType) -> TypeCheckEnv -> TypeCheckEnv
modifyParentType mpt te = (TE
	(mpt (parentType te))
	(compList te)
	(identType te))

modifyCompList mcl te = (TE
	(parentType te)
	(mcl (compList te))
	(identType te))

modifyIdentType mit te = (TE
	(parentType te)
	(compList te)
	(mit (identType te)))
-- }}}

-- TYPE DEF {{{
typeDef :: ETypeDef -> TypeCheckEnv -> TypeCheckEnv
typeDef etd tce = (compListDef etd (parentTypeDef etd tce))

compListDef :: ETypeDef -> TypeCheckEnv -> TypeCheckEnv
compListDef (ETypeDef eti ectl) tce = 
	foldl (\acc ect -> compListDefSingle ect acc) tce ectl

compListDefSingle :: EConstType -> TypeCheckEnv -> TypeCheckEnv
compListDefSingle (EConstTypes ident) = 
	modifyCompList (addCompList (Just []) ident)
compListDefSingle (EConstTypem ident l) =
	modifyCompList (addCompList (Just l) ident)

parentTypeDef :: ETypeDef -> TypeCheckEnv -> TypeCheckEnv
parentTypeDef (ETypeDef eti ectl) tce =
	foldl (\acc ect -> parentTypeDefSingle eti ect acc) tce ectl


parentTypeDefSingle :: ETypeIdent -> EConstType -> TypeCheckEnv -> TypeCheckEnv
parentTypeDefSingle pid (EConstTypes ident) = 
	modifyParentType (addParentType (Just pid) ident)
parentTypeDefSingle pid (EConstTypem ident _) = 
	modifyParentType (addParentType (Just pid) ident)
-- }}}


-- ADD {{{

addIdentType :: Maybe ECompType -> EIdent -> IdentType -> IdentType
addIdentType mtype ident it = (\i -> if i == ident then mtype else it i)

addParentType :: Maybe ETypeIdent -> ETypeConstIdent -> ParentType -> ParentType
addParentType mti tci pt = (\i -> if i == tci then mti else pt i)

addCompList :: Maybe [ECompType] -> ETypeConstIdent -> CompList -> CompList
addCompList mctl tci cl =
	(\i -> if i == tci then mctl else cl i)

addIdentTypeList :: [Maybe ECompType] -> [EIdent] -> IdentType -> IdentType
addIdentTypeList mtl idl it =
	let zipped = zipWith (\t i -> (t, i)) mtl idl in
	foldl (\it' (t, i) -> addIdentType t i it') it zipped
-- }}}

-- {{{
addMType :: Maybe ECompType -> EIdent -> TypeState -> TypeState
addMType mt eident ts = (\i -> if i == eident then mt else ts i)
	
addType :: ECompType -> EIdent -> TypeState -> TypeState
addType etype eident ts = (\i -> if i == eident then Just (etype) else ts i)
-- }}}

typeCheckMEStmt :: EStmt -> Reader TypeCheckEnv (Maybe ECompType)

typeCheckMEStmt (ELet (EDecl i v) s) = do
	vt <- typeCheckMEStmt v
	st <- local (modifyIdentType (addIdentType vt i)) (typeCheckMEStmt s)
	return (do
		vt' <- vt
		st' <- st
		return st')
		
typeCheckMEStmt (EIf c t f) = do
	tc <- typeCheckMEStmt c
	tt <- typeCheckMEStmt t
	tf <- typeCheckMEStmt f
	return (do
		tc' <- tc
		tt' <- tt
		tf' <- tf
		if tc' == ELitType (ETypeIdent "BOOL")
		then (if tt' == tf' then return tt' else Nothing)
		else Nothing)

typeCheckMEStmt (ELamb etype eident estmt) = do
	t <- local (modifyIdentType (addIdentType (Just etype) eident))
		(typeCheckMEStmt estmt)
	return (t >>= (\jt -> Just (EFunType (etype) jt)))


typeCheckMEStmt (EApp e1 e2) = do
	tf <- typeCheckMEStmt e1
	ta <- typeCheckMEStmt e2
	return (do
		tf' <- tf
		ta' <- ta
		case tf' of
			EFunType tfa tfv -> if tfa == ta' then return tfv else Nothing
			_ -> Nothing)

typeCheckMEStmt (EPar estmt) = do
	t <- typeCheckMEStmt estmt
	return t

typeCheckMEStmt (EAdd e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "INT")) else Nothing)))

typeCheckMEStmt (ESub e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "INT")) else Nothing)))

typeCheckMEStmt (EMul e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "INT")) else Nothing)))

typeCheckMEStmt (EDiv e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "INT")) else Nothing)))

typeCheckMEStmt (EAnd e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "BOOL")) && j2 == (ELitType (ETypeIdent "BOOL")) 
		then Just (ELitType (ETypeIdent "BOOL")) else Nothing)))

typeCheckMEStmt (EOr e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "BOOL")) && j2 == (ELitType (ETypeIdent "BOOL")) 
		then Just (ELitType (ETypeIdent "BOOL")) else Nothing)))

typeCheckMEStmt (ENot e1) = do
	t1 <- typeCheckMEStmt e1
	return (t1 >>= (\j1 -> 
		if j1 == (ELitType (ETypeIdent "BOOL")) 
		then Just (ELitType (ETypeIdent "BOOL")) else Nothing))

typeCheckMEStmt (ECmp e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "BOOL")) else Nothing)))

typeCheckMEStmt (ELes e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "BOOL")) else Nothing)))

typeCheckMEStmt (EGrt e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "BOOL")) else Nothing)))

typeCheckMEStmt (EGEq e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "BOOL")) else Nothing)))
typeCheckMEStmt (ELEq e1 e2) = do
	t1 <- typeCheckMEStmt e1
	t2 <- typeCheckMEStmt e2
	return (t1 >>= (\j1 -> t2 >>= (\j2 ->
		if j1 == (ELitType (ETypeIdent "INT")) && j2 == (ELitType (ETypeIdent "INT")) 
		then Just (ELitType (ETypeIdent "BOOL")) else Nothing)))

typeCheckMEStmt (EBool _) = do
	return $ Just $ ELitType (ETypeIdent "BOOL")

typeCheckMEStmt (EInt _) = do
	return $ Just $ ELitType (ETypeIdent "INT")

typeCheckMEStmt (EVar eident) = do
	s <- (ask)
	return ((identType s) eident)

typeCheckMEStmt (EInstS etci) = do
	s <- (ask)
	return (((parentType s) etci) >>= (\p' -> ((compList s) etci) >>= (\l -> if l == [] then Just (ELitType(p')) else Nothing)))

typeCheckMEStmt (EInstM etci l) = do
	s <- ask
	actualTypeList <- mapM (typeCheckMEStmt) l
	return (if (sequence actualTypeList) == ((compList s) etci) 
		then ((parentType s) etci) >>= (\p' -> Just (ELitType(p')))
		else Nothing)

typeCheckMEStmt (ECase stmt matchlist) = do
	mt <- typeCheckMEStmt stmt --jeden typ - to co maczujemy
	ctl <- mapM (typeCheckMEPattern) matchlist -- typy patternow [Maybe (T)]
	tl <- mapM (typeCheckMEMatch) matchlist
	return
		(mt >>= (\mt' ->
		((sequence ctl) >>= (\ctl' ->
		((sequence tl) >>= (\tl' ->
		((isSame (tl')) >>= (\t ->
		(if (all (==mt') ctl') then Just t else Nothing)))))))))


-- typy patternow
typeCheckMEPattern :: EMatch -> Reader TypeCheckEnv (Maybe ECompType)

typeCheckMEPattern (EMatchS cid estmt) = do
	s <- ask
	return (((parentType s) cid) >>= (\t -> Just (ELitType t)))

typeCheckMEPattern (EMatchM cid idlist estmt) = do
	s <- ask
	return (((parentType s) cid) >>= (\t -> Just (ELitType t)))


--typy wyrazen
typeCheckMEMatch :: EMatch -> Reader TypeCheckEnv (Maybe ECompType)

typeCheckMEMatch (EMatchS cid estmt) = do
	t <- typeCheckMEStmt estmt
	return t
typeCheckMEMatch (EMatchM cid idlist estmt) = do
	s <- ask
	st <- local (modifyIdentType (addIdentTypeList (pushMaybe ((compList s) cid)) (idlist))) (typeCheckMEStmt estmt)
	return st

